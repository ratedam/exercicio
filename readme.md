- Put the files on a base location (example: localhost instead of localhost/exercise )
- Create a new database
- Execute the mysql statement to create the products table

CREATE TABLE products (    
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    sku varchar(255) UNIQUE NOT NULL,
    quantity int,
    PRIMARY KEY (id) )

 - Change the file models/DatabaseConnection $connection array to your localhost configuration

 - Access the folder and navigate through the website