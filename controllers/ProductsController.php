<?php
	
	include "models/Product.php"; 

	/*
		Product routes controller
	*/
	class ProductsController{

		/*
			Product listing
		*/
		public static function index(){
			$products = new Product();
			$products = $products->getAll();
			include('views/index.php');
		}


		/*
			Product creation page
		*/
		public static function create(){
			include('views/create.php');
		}

		/*
			Product edit page
		*/
		public static function edit(){
			//get the id
			$id = str_replace('/products/edit/','',$_SERVER['REQUEST_URI']);
			if( is_numeric ( $id ) ){
				$product = new Product();

				$product = $product->get( $id );

				include('views/edit.php');

			}else{
				header('Location: /?status=500&message=Element not found' );
			}
		}

		/*
			Product update page
		*/
		public static function update(){
			//get the id
			$id = str_replace('/products/update/','',$_SERVER['REQUEST_URI']);

			if( is_numeric ( $id ) ){
				$product = new Product();

				$updated_product = $product->update( $id, [ 'name' => $_POST['name'] , 'quantity' => $_POST['quantity'] ] );
				
				header('Location: /?status='. $updated_product['status'] .'&message=' . $updated_product['message'] );


			}else{
				header('Location: /?status=500&message=Element not found' );
			}
		}

		/*
			Product delete page
		*/
		public static function destroy(){
			//get the id
			$id = str_replace('/products/delete/','',$_SERVER['REQUEST_URI']);

			if( is_numeric ( $id ) ){
				$product = new Product();

				$deleted_product = $product->destroy( $id );
				
				header('Location: /?status='. $deleted_product['status'] .'&message=' . $deleted_product['message'] );


			}else{
				header('Location: /?status=500&message=Element not found' );
			}
		}


		/*
			Product create page
		*/
		public static function store(){
			if( isset( $_POST['name'] ) && ! empty( $_POST['name'] )){

				$products = new Product();
					$created_product = $products->create([
						'name' => $_POST['name'],
						'quantity' => ( isset( $_POST['quantity'] ) ) ? $_POST['quantity'] : 0
					]);
					
					header('Location: /?status='. $created_product['status'] .'&message=' . $created_product['message'] );
			}else{
				header('Location: /?status=500&message=Please insert a valid name' );
			}
		}
	}

?>