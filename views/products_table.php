

<div class="row">

	<?php if( count( $products ) > 0 ) :?>
		<table class="table table-striped">
			<tr>
				<th>Name</th>
				<th>SKU</th>
				<th>Quantity</th>
				<th></th>
			</tr>

			<?php foreach( $products as $product ) : ?>
				<tr>
					<td><?php echo $product['name']; ?></td>
					<td><?php echo $product['sku']; ?></td>
					<td><?php echo $product['quantity']; ?></td>
					<td>
						<a href="products/edit/<?php echo $product['id']; ?>">Edit</a>
						|
						<a href="products/delete/<?php echo $product['id']; ?>">Delete</a>
					</td>
				</tr>
			<?php endforeach; ?>
			
		</table>

		
	<?php else: ?>
		<div class="alert alert-danger" role="alert">No products found. Please insert one by using the form below.</div>
	<?php endif; ?>



	
</div>