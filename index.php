<?php
	require "controllers/ProductsController.php";


	// Our route control handler
	$route = $_SERVER['REQUEST_URI'];
	$method = $_SERVER['REQUEST_METHOD'];

	if( $method === 'GET' ){
		switch( $route ){
			case '/products/all' :
				ProductsController::index();
				break;
			case '/products/create' :
				ProductsController::create();
				break;
			case strpos( $route , '/products/edit') :
				ProductsController::edit();
				break;
			case strpos( $route , '/products/delete') :
				ProductsController::destroy();
				break;
			default:
				ProductsController::index();
				break;
		}
	}elseif( $method === 'POST' ){
		switch( $route ){
			case '/products/create' :
				ProductsController::store();
				break;
			case strpos( $route , '/products/update') :
				ProductsController::update();
				break;
			default:
				break;
		}
	}


?>
