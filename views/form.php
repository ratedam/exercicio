
<form action="/products/<?php echo isset( $product ) ? 'update/' . $product['id'] : 'create'; ?>" method="post">

  <div class="form-group">
    <label for="product-name">Product name</label>
    <input type="text" class="form-control" id="product-name" name="name" required value="<?php echo isset( $product['name'] ) ? $product['name'] : ''?>">
  </div>
  
  <div class="form-group">
    <label for="product-quantity">Quantity</label>
    <input type="number" min="0" class="form-control" id="product-quantity" name="quantity" value="<?php echo isset( $product['quantity'] ) ? $product['quantity'] : ''?>">
  </div>
  
  
  <button type="submit" class="btn btn-default pull-right">Submit</button>
</form>
