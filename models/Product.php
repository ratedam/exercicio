<?php

include "DatabaseConnection.php";


class Product extends DatabaseConnection{


	/*
		Product Model.
		Responsible to make all the products CRUD.
	*/



	// table name
	protected $table = 'products';


	function __construct(){
		parent::__construct();		
	}

	/*
		Get all the products
	*/
	function getAll(){

		$data = [];

		$result = $this->db_connection->query("SELECT * FROM ". $this->table );

		while ($row = $result->fetch()) {
			$data[] = [
				'id' => $row['id'],
				'name' => $row['name'],
				'quantity' => $row['quantity'],
				'sku' => $row['sku']
			];
		}


		return $data;
		
	}


	/*
		Get a single product
	*/
	function get( $id ){
		$data = [];

		$statement = $this->db_connection->prepare("SELECT * FROM ". $this->table . " WHERE id = :id" );

		$statement->execute( [ 'id' => $id ] );

		$row = $statement->fetch();

		if( $row ){
			$data = [
					'id' => $row['id'],
					'name' => $row['name'],
					'quantity' => $row['quantity'],
					'sku' => $row['sku']
				];
		}

		
		return $data;
	}



	/*
		Create a new product
	*/
	function create( $data ){

		// Field name is required
		if( !isset( $data['name'] ) ){
			return [
				'status' => 500,
				'message' => 'Name is required'
			];
		}

		// If no SKU was provided auto generate it
		$data['sku'] = $this->generateProductSKU( $data['name'] );

		// Prepare and insert it
		$statement = $this->db_connection->prepare(
						"INSERT INTO ". $this->table ." (name, sku, quantity)	
						VALUES(:name, :sku, :quantity)"
					);

		// Insert		
		$statement->execute([
			'name' => $data['name'],
			'sku' =>  $data['sku'],
			'quantity' => $data['quantity'],
			
		]);		


		// Everything OK continue
		return [
			'status' => 200,
			'message' => 'Created with success!'
		];
	}



	/*
		Update a product
	*/
	function update( $id , $data ){

		// Field name is required
		if( !isset( $data['name']) ){
			return [
				'status' => 500,
				'message' => 'Name is required'
			];
		}

		// Field quantity is required
		if( !isset( $data['quantity']) ){
			return [
				'status' => 500,
				'message' => 'Quantity is required'
			];
		}
		
		$current_product_details = $this->get($id);
		
		if( $data['name'] !== $current_product_details['name'] ){
			$data['sku'] = $this->generateProductSKU( $data['name'] );
		}else{
			$data['sku'] = $current_product_details['sku'];
		}
		

		$result = $this->db_connection->prepare("UPDATE " . $this->table . " SET name = :name, quantity = :quantity, sku = :sku WHERE id = :id");
		

		$result->execute([
			'name' => $data['name'],
			'quantity' => $data['quantity'],
			'id' => $id,
			'sku' => $data['sku']
		]);


		return [
			'status' => 200,
			'message' => 'Updated with success!'
		]; 
		
	}


	/*
		Destroy a product
	*/
	function destroy( $id ){
		$result = $this->db_connection->prepare("DELETE FROM ". $this->table ." WHERE id = :id");

		$result->execute([
			'id' => $id
		]); 

		return [
			'status' => 200,
			'message' => 'Deleted with success'
		];
	}

	/*
		Generate the product unique SKU
	*/
	private function generateProductSKU( $name ){
		$sku_exists = true;

		$possible_sku = $this->normalize_string( $name );

		while( $sku_exists ){

			$result = $this->db_connection->prepare("SELECT count(sku) FROM ". $this->table ." WHERE sku = :sku");

			$result->execute([
				'sku' => $possible_sku
			]); 

			if( $result->fetchColumn() == 0 ){
				$sku_exists = false;
			}else{
				$possible_sku .= '-1';
			}
		}


		return $possible_sku;
	}


	/*
		Remove all the accents, special characters and spaces to make a unique SKU
	*/
	private function normalize_string( $string){
		

		$normalizeChars = array(
		    'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
		    'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
		    'Ï'=>'I', 'Ñ'=>'N', 'Ń'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
		    'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
		    'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
		    'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ń'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
		    'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f',
		    'ă'=>'a', 'î'=>'i', 'â'=>'a', 'ș'=>'s', 'ț'=>'t', 'Ă'=>'A', 'Î'=>'I', 'Â'=>'A', 'Ș'=>'S', 'Ț'=>'T',
		);
		
	
		return strtolower(preg_replace("/[^A-Za-z0-9 ]/", '_', str_replace(' ', '_', strtr($string, $normalizeChars) )));
	}
	
}


?>
